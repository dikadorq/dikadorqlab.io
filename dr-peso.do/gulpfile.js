let { src, dest } = require("gulp");
let gulp = require("gulp");
let sass = require("gulp-sass")(require("node-sass"));
let autoprefixer = require("gulp-autoprefixer");
let sourcemaps = require("gulp-sourcemaps");
let cssmin = require("gulp-cssmin");
let rename = require("gulp-rename");
let concat = require("gulp-concat");
const fileinclude = require("gulp-file-include");
const browsersync = require("browser-sync").create();

function html() {
   return src(["./src/html/*.html"])
      .pipe(
         fileinclude({
            prefix: "@@",
            basepath: "@file",
         })
      )
      .pipe(dest("./dist/"))
      .pipe(browsersync.stream());
}

function styles() {
   return src(["./src/css/noauth/sass.scss"])
      .pipe(sourcemaps.init())
      .pipe(sass())
      .pipe(
         autoprefixer({
            grid: true,
            overrideBrowserslist: ["last 5 versions"],
            cascade: true,
         })
      )
      .pipe(cssmin())
      .pipe(rename({ suffix: ".min" }))
      .pipe(sourcemaps.write("maps/"))
      .pipe(dest("./dist/css/noauth/"))
      .pipe(browsersync.stream());
}

function browserSync(params) {
   browsersync.init({
      server: {
         baseDir: "./dist",
      },
      port: 3000,
      notify: false,
   });
}

function cssLib() {
   return src(["./src/css/*.css"]).pipe(dest("./dist/css/")).pipe(browsersync.stream());
}

function jsLibrary() {
   return src([
      "./dist/lib/main/jquery.min.js",
      "./dist/lib/main/jquery-ui.js",
      "./dist/lib/main/parsley.min.js",
      "./dist/lib/main/nouislider.min.js",
      "./dist/lib/main/wNumb.js",
      "./dist/lib/main/jquery.maskedinput.min.js",
      "./dist/lib/main/owl.carousel.js",
   ])
      .pipe(concat("library_js.js"))
      .pipe(dest("./dist/js/"));
}

function js() {
   return src("./src/js/*.js").pipe(dest("./dist/js/")).pipe(browsersync.stream());
}

function images() {
   return src("./src/images/**").pipe(dest("./dist/images/")).pipe(browsersync.stream());
}

function watchFiles(params) {
   gulp.watch(["./src/css/noauth/part/**/*"], styles);
   gulp.watch(["./src/html/*.html"], html);
   gulp.watch(["./src/js/*.js"], js);
   gulp.watch(["./src/images/**"], images);
}

let build = gulp.series(gulp.parallel(cssLib, styles, html, images, js, jsLibrary));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.styles = styles;
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;
